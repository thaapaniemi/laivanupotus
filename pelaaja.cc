//*********************************************************************
// 199006 Tomi Haapaniemi
// haapani8
// 
// Ohjelmointi 2e
// Harjoitusty�
// 
// Pelaaja-luokan toteutus
// huolehtii pelaajan toiminnoista (laivat, pommitukset, kartat, yms)
//*********************************************************************

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "laiva.hh"
#include "pelaaja.hh"
#include "yleisfunktiot.hh"

// Funktio: Pelaaja();
// Parametrit: -
// Toiminta: Oletusrakentaja
// Paluuarvo: -
Pelaaja::Pelaaja():nimi_(),koko_(0),vakoiltu_(false){}

// Funktio: Pelaaja(const string& unimi, const unsigned int& ukoko);
// Parametrit: pelaajan nimi, kartan koko
// Toiminta: Rakentaja, joka alustaa pelaajalle nimen ja kartan koon
// Paluuarvo: -
Pelaaja::Pelaaja(const string& unimi, const unsigned int& ukoko):
nimi_(unimi),koko_(ukoko),vakoiltu_(false){}

// Funktio: ~Pelaaja();
// Parametrit: -
// Toiminta: Purkaja luokalle
// Paluuarvo: -
Pelaaja::~Pelaaja(){
   // poistetaan kaikki laivat kartasta
   while( !laivat_.empty() ) {
      delete laivat_.begin()->second;
      laivat_.begin()->second = 0;
      laivat_.erase( laivat_.begin() );
   }
}

// Funktio: bool lisaaLaiva(Laiva* laiva);
// Parametrit: pointteri lis�tt�v��n laivaan
// Toiminta: Lis�� laivan pelaaja-olioon
// Paluuarvo: onnistuiko lis�ys
bool Pelaaja::lisaaLaiva(Laiva* laiva){
   Laiva::Koordinaatit lK = laiva->sijainti();
   
   
   // Tapaus keulan x == per�n x -> laiva on pystyss�
   if(lK.kx == lK.px){
      if(lK.ky >= lK.py){
         for(unsigned int i = 0; i <= (lK.ky - lK.py); ++i){
            // Lis�t��n koordinaatistoon laivan koordinaattipiste
            if(!lisaaKoordinaatti_(AvainKoordinaatti_ (lK.kx, (lK.ky-i)), 
                                   Ruutu_ (laiva, false))){
               // koordinaateissa on jo laiva -> laivatiedosto on viallinen
               return false; 
               
            }
         }
      }
      else if(lK.ky < lK.py){
         for(unsigned int i = 0; i <= (lK.py - lK.ky); ++i){
            // Lis�t��n koordinaatistoon laivan koordinaattipiste
            if(!lisaaKoordinaatti_(AvainKoordinaatti_ (lK.kx, (lK.py-i)), 
                                   Ruutu_ (laiva, false))){
               // koordinaateissa on jo laiva -> laivatiedosto on viallinen
               return false; 
            }
         }
      }
   }
   // Tapaus keulan y == per�n y -> laiva on vaakassa
   else if(lK.ky == lK.py){
      if(lK.kx >= lK.px){
         for(unsigned int i = 0; i <= (lK.kx - lK.px); ++i){
            // Lis�t��n koordinaatistoon laivan koordinaattipiste
            if(!lisaaKoordinaatti_(AvainKoordinaatti_ ((lK.kx-i), lK.ky), 
                                   Ruutu_ (laiva, false))){
               // koordinaateissa on jo laiva -> laivatiedosto on viallinen
               return false;
            }
         }
      }
      else if(lK.kx < lK.px){
         for(unsigned int i = 0; i <= (lK.px - lK.kx); ++i){
            // Lis�t��n koordinaatistoon laivan koordinaattipiste
            if(!lisaaKoordinaatti_(AvainKoordinaatti_ ((lK.px-i), lK.ky), 
                                   Ruutu_ (laiva, false))){
               // koordinaateissa on jo laiva -> laivatiedosto on viallinen
               return false;
            }
         }
      }
   }			
   
   // Lis�t��n laiva laivalistaan
   string laivannimi = laiva->laivanNimi();
   yf::pieniksi(laivannimi);
   laivat_.insert(make_pair(laivannimi, laiva));
   
   return true;
}


// Funktio: void tulostaItselle() const;
// Parametrit: -
// Toiminta: tulostaa OMA-komentoa vastaavan kartan cout-virtaan
// Paluuarvo: -
void Pelaaja::tulostaItselle() const{
   vector<string> tulostusvektori;
   lisaaNumerointi_('~', tulostusvektori);
   
   // Lis�t��n laivat ja pommitukset
   for(const_itKoord i = koordinaatisto_.begin(); i != koordinaatisto_.end();
       ++i){
      // Kun ruutua on pommitettu sek� siin� on laiva jolla on kestoa j�ljell�
      if(i->second.pommitettu == true && i->second.laiva != 0 
         && i->second.laiva->jaljella() != 0){
         tulostusvektori.at(i->first.y + 2).at(i->first.x + 2) = 'X';
      }
      // Kun ruutua on pommitettu sek� siin� on laiva jolla ei ole 
      // kestoa j�ljell�
      else if(i->second.pommitettu == true && i->second.laiva != 0 
              && i->second.laiva->jaljella() == 0){
         tulostusvektori.at(i->first.y + 2).at(i->first.x + 2) = '#';
      }
	  // Kun ruutua ei ole pommitettu ja siin� on laiva
      else if(i->second.pommitettu == false && i->second.laiva != 0){
         tulostusvektori.at(i->first.y + 2).at(i->first.x + 2) = '@';
      }
   }
   
   //Suoritetaan tulostus
   for(unsigned int i = 0; i < tulostusvektori.size(); ++i){
      cout << tulostusvektori.at(i) << endl;
   }
}

// Funktio: void tulostaVastustajalle() const;
// Parametrit: -
// Toiminta: tulostaa KARTTA-komentoa vastaavan kartan cout-virtaan
// Paluuarvo: -
void Pelaaja::tulostaVastustajalle() const{
   
   vector<string> tulostusvektori;
   
   // Suoritetaan numerointi kartan reunoille sek� viivat
   lisaaNumerointi_('.', tulostusvektori);
   
   for(const_itKoord i = koordinaatisto_.begin(); 
       i != koordinaatisto_.end(); ++i){
      
      // Ruutu johon on ammuttu, mutta ei ole laivaa
      if(i->second.pommitettu == true && i->second.laiva == 0){
         tulostusvektori.at(i->first.y + 2).at(i->first.x + 2) = '~';
      }
      // Ruutu johon ammuttu, on laiva, muttei uponnut
      else if(i->second.pommitettu == true && i->second.laiva != 0 
              && i->second.laiva->jaljella() != 0){
         tulostusvektori.at(i->first.y + 2).at(i->first.x + 2) = 'X';
      }
      // Ruutu johon ammuttu sek� laiva uponnut
      else if(i->second.pommitettu == true && i->second.laiva != 0 
              && i->second.laiva->jaljella() == 0){
         tulostusvektori.at(i->first.y + 2).at(i->first.x + 2) = '#';
      }
   }
   
   //Suoritetaan tulostus
   for(unsigned int i = 0; i < tulostusvektori.size(); ++i){
      cout << tulostusvektori.at(i) << endl;
   }
   
}

// Funktio: bool pommita(unsigned int& x, unsigned int& y);
// Parametrit: pommitettavan ruudun x ja y-koordinaatit
// Toiminta: pommitaa annettua ruutua
// Paluuarvo: onnistuiko pommitus
bool Pelaaja::pommita(int& x, int& y){
   // Tarkastetaan ett� koordinaatit on kartan sis�ll�
   if(x < 0 || static_cast<unsigned int>(x) >= koko_ || y < 0 
      || static_cast<unsigned int>(y) >= koko_){
      // T�h�n virhetulostus
      cerr << "Virhe: Virheelliset koordinaatit." << endl;
      return false;
   }
   AvainKoordinaatti_ ak (x, y);
   if(koordinaatisto_.count(ak) != 0 
      && koordinaatisto_[ak].pommitettu == false 
      && koordinaatisto_[ak].laiva != 0){
      
      koordinaatisto_[ak].pommitettu = true;
      if(!koordinaatisto_[ak].laiva->pommita(x,y)){
         // T�m�n pit�isi onnistua tai jossain on vikaa
         cerr << "t�t� ei pit�isi tulla" << endl;
         return false;
      }
      // Jos laivalla ei ole en�� ruutuja j�ljell� niin laiva upposi
      if(koordinaatisto_[ak].laiva->jaljella() == 0){
         cout << "* * * OSUI JA UPPOSI! * * *" << endl;
      }
      // muuten vain osutaan
      else{
         cout << "* * * OSUI! * * *" << endl;
      }
      
      return true;
   }
   // Tarkastetaan onko ruutua jo pommitettu
   else if(koordinaatisto_.count(ak) != 0 
           && koordinaatisto_[ak].pommitettu == true){
      // T�t� ruutua on pommitettu
      cerr << "Ruutuun on pommitettu jo!" << endl;		
      return false;
   }
   else{
      // Lis�t��n veteen pommitus koordinaatistoon
      koordinaatisto_.insert(make_pair(ak, Ruutu_ (0,true)));
      cout << "-- OHI! --" << endl;
      return true;
   }
   
   return false;
}


// Funktio: void lisaaNumerointi_(char merkki, 
//          vector<string>& tulostusvektori) const;
// Parametrit: merkki jolla tulostusvektori t�ytet��n, tulostusvektori
// Toiminta: luo tulostusvektorin jossa on koordinaatisto reunalla ja on 
//            muuten t�ytetty annetulla merkill�
// Paluuarvo: -
void Pelaaja::lisaaNumerointi_(char merkki, vector<string>& tulostusvektori)
  const{
     
     // Luodaan vetinen kartta
     for(unsigned int i = 0; i < koko_ + 2; ++i){
        tulostusvektori.push_back(string(koko_ + 2, merkki));
     }
     
     // Laitetaan viivat kohdilleen
     tulostusvektori.at(1) = string(tulostusvektori.at(1).size(), '-');
     
     for(unsigned int i = 1; i < tulostusvektori.size(); ++i){
        tulostusvektori.at(i).at(1) = '|';
     }
     
     
     // Ulostusasun s��t��
     tulostusvektori.at(0).at(0) = ' ';
     tulostusvektori.at(0).at(1) = ' ';
     tulostusvektori.at(1).at(0) = ' ';
     tulostusvektori.at(1).at(1) = '+';
     
     // Laitetaan pystyrivien numerot
     for(unsigned int i = 2; i < tulostusvektori.at(0).size(); ++i){
        // muutetaan numero chariksi ja muutetaan merkki�
        tulostusvektori.at(0).at(i) = static_cast<char>(((i - 2) % 10) + 48);
     }
     
     // Laitetaan vaakarivien numerot
     for(unsigned int i = 2; i < tulostusvektori.size(); ++i){
        // muutetaan numero chariksi ja muutetaan merkki�
        tulostusvektori.at(i).at(0) = static_cast<char>(((i - 2) % 10) + 48);
      }
     
  }

// Funktio: bool lisaaKoordinaatti_(AvainKoordinaatti_ kpiste, Ruutu_ ruutu);
// Parametrit: koordinaattipistestructi, pisteen tiedot
// Toiminta: lis�� uuden koordinaattiruudun karttaan
// Paluuarvo: onnistuiko lis�ys
bool Pelaaja::lisaaKoordinaatti_(AvainKoordinaatti_ kpiste, Ruutu_ ruutu){
   
   // T�h�n tarkistus p��llekk�isyydest�
   for(itKoord i = koordinaatisto_.begin(); i != koordinaatisto_.end(); ++i){
      if(i->first.x == kpiste.x && i->first.y == kpiste.y){
         return false;
      }
   }
   
   // Lis�t��n piste koordinaatistoon
   koordinaatisto_.insert(make_pair(kpiste, ruutu));
   
   return true;
}

// Funktio: unsigned int koko() const;
// Parametrit: -
// Toiminta: -
// Paluuarvo: palauttaa kartan koon
unsigned int Pelaaja::koko() const{
   return koko_;
}

// Funktio: string nimi() const;
// Parametrit: -
// Toiminta: -
// Paluuarvo: palauttaa pelaajan nimen
string Pelaaja::nimi() const{
   return nimi_;
}

// Funktio: void tiedot() const;
// Parametrit: -
// Toiminta: tulostaa tiedot kaikista laivoista
// Paluuarvo: -
void Pelaaja::tiedot() const{
   for(const_itLaiva i = laivat_.begin(); 
       i != laivat_.end(); ++i){
      
      // haetaan laivan sijainti
      Laiva::Koordinaatit sijainti = i->second->sijainti();
      
      // Tulostetaan tiedot
      cout << i->second->laivanNimi() << " (" << sijainti.kx << "," 
        << sijainti.ky << ") ";
      cout << i->second->jaljella() << "/" << i->second->pituus() << endl;
      
      cout << "Alaiset:" << endl;
      // K�sket��n laivaa tulostamaan alaiset
      i->second->tulostaAlaiset();
      
      cout << endl;
   }
}

// Funktio: bool tiedot(const string& parametri) const;
// Parametrit: laivan nimi tai sen osa
// Toiminta: etsii laivan annetulla parametrilla ja jos laiva l�ytyy tulostaa
//           sen tiedot
// Paluuarvo: l�ytyik� laiva
bool Pelaaja::tiedot(const string& parametri) const{
   
   bool onkoLaiva = false;
   
   
   // K�yd��n kaikki laivat l�vitse
   for(const_itLaiva i = laivat_.begin(); 
       i != laivat_.end(); ++i){
      
      // mapissa oleva avainstringi on jo pienen� joten sit� ei tarvitse
      // muuttaa uudelleen
      string lnimi = i->first;
      
      string vertailtava(parametri);		
		yf::pieniksi(vertailtava);
      
      
      if(lnimi.find(vertailtava) != string::npos){
         onkoLaiva = true;
         
         // haetaan laivan sijainti
         Laiva::Koordinaatit sijainti = i->second->sijainti();
         
         // Tulostetaan tiedot
         cout << i->second->laivanNimi() << " (" << sijainti.kx << "," 
           << sijainti.ky << ") ";
         cout << i->second->jaljella() << "/" << i->second->pituus() << endl;
         
         cout << "Alaiset:" << endl;
         // K�sket��n laivaa tulostamaan alaiset
         i->second->tulostaAlaiset();
         
         cout << endl;
		 }
      
   }
   
   
   if(!onkoLaiva){
      cout << "Sinulla ei ole sen nimisi� laivoja." << endl;
      return false;
   }
   
   return true;
}

// Funktio: unsigned int ruutuja() const;
// Parametrit: -
// Toiminta: laskee pelaajan laivaruutujen kokonaism��r�n
// Paluuarvo:  pelaajan laivaruutujen kokonaism��r�
unsigned int Pelaaja::ruutuja() const{
   unsigned int yhteensa = 0;
   for(const_itLaiva i = laivat_.begin(); 
       i != laivat_.end(); ++i){
      yhteensa += i->second->pituus();
   }
   
   return yhteensa;
}

// Funktio: unsigned int ehjia() const;
// Parametrit: -
// Toiminta: laskee pelaajan ehjien laivaruutujen m��r�n
// Paluuarvo: pelaajan ehjien laivaruutujen m��r�
unsigned int Pelaaja::ehjia() const{
   unsigned int ehjiaRuutuja = 0;
   for(const_itLaiva i = laivat_.begin(); 
       i != laivat_.end(); ++i){
      ehjiaRuutuja += i->second->jaljella();
   }
   
   return ehjiaRuutuja;
}

// Funktio: unsigned int laivojaJaljella() const;
// Parametrit: -
// Toiminta: laskee ei-uponneiden laivojen m��r�n
// Paluuarvo: ei-uponneiden laivojen m��r�
unsigned int Pelaaja::laivojaJaljella_() const{
   unsigned int elaivoja = 0;
   // K�yd��n l�pi kaikki laivat ja katsotaan montako ehj�� laivaa l�ytyy
   for(const_itLaiva i = laivat_.begin(); 
       i != laivat_.end(); ++i){
      if(i->second->jaljella() > 0){
         ++elaivoja;
      }
   }
   
   return elaivoja;
}

// Funktio: bool tulostaKomentoketju(string parametri) const;
// Parametrit: laivan nimi
// Toiminta: tulostaa cout-virtaan annetun laivan komentoketjun
// Paluuarvo: l�ytyik� laivaa
bool Pelaaja::tulostaKomentoketju(string parametri) const{
   // pienennet��n parametri
   yf::pieniksi(parametri);
   
   // katsotaan l�ytyyk� laivaa
   if(laivat_.count(parametri) != 0){
      
      // kai t�h�nkin l�ytyy parempi tapa saada toimimaan constin kanssa
      // jos l�ytyy niin tulostetaan laiva
      laivat_.find(parametri)->second->tulostaKomentoketju();
      
      return true;
   }
   else{
      // ilmoitetaan ettei laivaa l�ytynyt
      cout << "Sinulla ei ole " << parametri << " nimist� laivaa." << endl;
   }
   
   return false;
}

// Funktio: bool vakoile(unsigned int kx, unsigned int ky);
// Parametrit: vakoiltavan ruudun x ja y-koordinaatit
// Toiminta: vakoilee annettua ruutua
// Paluuarvo: onnistuiko vakoilu
bool Pelaaja::vakoile(int& kx, int& ky){
   
   // tarkistetaan vakoilun tilanne
   if(vakoiltu_ == true){
      cout << "Et voi vakoilla toista kertaa." << endl;
      return false;
   }
   // tarkastetaan koordinaatit
   else if(kx < 0 || static_cast<unsigned int>(kx) >= koko_ || ky < 0 
          || static_cast<unsigned int>(ky) >= koko_){
      cout << "Virhe: Virheelliset koordinaatit." << endl;
      return false;
   }
   
   // luodaan vertailuavain
   AvainKoordinaatti_ avain(kx, ky);
   
   // tarkistetaan l�ytyyk� vertailuavainta
   if(koordinaatisto_.count(avain) != 0){
      
      // Haetaan avainkoordinaattia koordinaatistosta
      // jos  koordinaattipiste l�ytyy mutta siin� ei ole laivaa eli sit� on
      // pommitettu tai jos siin� on laiva ja sit� on pommitettu
      if(koordinaatisto_[avain].laiva == 0 
         || koordinaatisto_[avain].pommitettu == true){
         cout << "Ruutua on turha vakoilla." << endl;
         return false;
      }
      
      // muissa tapauksissa tulostetaan komentoketju
      koordinaatisto_[avain].laiva->tulostaKomentoketju();
      vakoiltu_ = true;
      
      return true;
	}
   
   // jos pistett� ei l�ydy niin vakoilu ei tuota tulosta
   cout << "Vakoiluoperaatio ei tuottanut tulosta." << endl;
   vakoiltu_ = true;
   
   
   return true;
   
}

// Funktio: unsigned int tulostaTilasto() const;
// Parametrit: -
// Toiminta: tulostaa cout-virtaan pelaajan tilastotiedot
// Paluuarvo: pelaajan ehjien ruutujen lukum��r�
unsigned int Pelaaja::tulostaTilasto() const {
   cout << nimi() << ", Laivoja j�ljell�: " << laivojaJaljella_() 
     << ", Osumakohtia: " << ehjia() << "/" <<  ruutuja() << endl;
   
   return ehjia();
   
}

//Pelaaja::AvainKoordinaatti_-structin rakentaja
Pelaaja::AvainKoordinaatti_::AvainKoordinaatti_(){}

//Pelaaja::AvainKoordinaatti_-structin rakentaja
Pelaaja::AvainKoordinaatti_::AvainKoordinaatti_(unsigned int ax, 
                                 unsigned int ay):x(ax),y(ay){}
//Pelaaja::AvainKoordinaatti_-structin vertailuoperaattori
bool Pelaaja::AvainKoordinaatti_::operator ()(
                            const Pelaaja::AvainKoordinaatti_& a1, 
                            const Pelaaja::AvainKoordinaatti_& a2) const{
   if(a1.y > a2.y || a1.y == a2.y && a1.x > a2.x){
      return true;
   }
                               
   return false;
}
