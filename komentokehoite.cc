//*********************************************************************
// 199006 Tomi Haapaniemi
// haapani8
// 
// Ohjelmointi 2e
// Harjoitusty�
// 
// KomentoKehoite-moduuli
// huolehtii komentokehoitteen toiminnasta
//*********************************************************************

#include <iostream>
#include <cstdlib>
#include <vector>
#include<list>
#include <sstream>
#include <string>
#include <cctype>
#include "komentokehoite.hh"
#include "pelaaja.hh"
#include "yleisfunktiot.hh"


using namespace std;

const string VIRHE = "Virhe: ";
const string KOMENTOVIRHE = "Virheellinen komento.";
const string LPARAMETRIVIRHE = "Liikaa parametreja.";
const string LVPARAMETRIVIRHE = "Liian v�h�n parametreja.";
const string KOKLUKUPVIRHE = " ei ole tulkittavissa kokonaislukuna.";

// Funktio: bool lueKomento(Kasky& komento, vector <string>& parametrit);
// Parametrit: komento ja parametrit
// Toiminta: lukee komennon ja parametrit
// Paluuarvo: onnistuiko luku
bool koke::lueKomento(Kasky& komento, vector <string>& parametrit){
   string syote = "";
   
   //Luetaan sy�te
   if(!getline(cin, syote))
   {
      return false;
   }
   else{
      string sana = "";
      istringstream virta(syote);
      // luetaan komento
      if(virta >> sana){
         komento = koke::tarkistaKomento(sana);
         
         //luetaan parametrit vektoriin
         while( virta >> sana){
            parametrit.push_back(sana);
         }
      }
      
      else{
         komento = TYHJA;
      }
   }
   
   return true;
}

// Funktio: Palautus suoritaKomento(unsigned int& kierros, 
//          list<Pelaaja*>& pelaajat, list<Pelaaja*>& kaikkiPelaajat, 
//          Kasky& komento, vector<string>& parametrit);
// Parametrit: mones kierros, pelaajalista, komento ja komennon parametrit
// Toiminta: suorittaa komennon
// Paluuarvo: mit� tehd��n seuraavaksi (jatketaan samalla pelaajalla, 
// vaihdetaan pelaajaa tai lopetetaan)
koke::Palautus koke::suoritaKomento(unsigned int& kierros, list<Pelaaja*>& pelaajat, 
                        list<Pelaaja*>& kaikkiPelaajat, Kasky& komento, 
                        vector <string>& parametrit){
   
   koke::tarkistaParametrit(komento, parametrit);
   
   switch(komento){
      
      //N�iss� tapauksissa ei tarvitse oikeastaan tehd� mit��n
    case KLPVIRHE: 
    case LOPETA:{
       break;
    }
      
      //Komentovirheen sattuessa
    case KVIRHE:{
       cerr << VIRHE << KOMENTOVIRHE << endl;
       break;
    }
      
      // Liian paljon Parametrivirheen sattuessa
    case LPVIRHE:{
       cerr << VIRHE << LPARAMETRIVIRHE << endl;
       break;
    }
      // Liian v�h�n Parametrivirheen sattuessa
    case LVVIRHE:{
       cerr << VIRHE << LVPARAMETRIVIRHE << endl;
       break;
    }
      
    case TIEDOT:{
       // Tyhj�n vektorin sattuessa sy�tet��n tyhj� stringi
       if(parametrit.size() == 0){
          pelaajat.front()->tiedot();
       }
       // muussa tapauksessa sy�tet��n parametri
       else{
          pelaajat.front()->tiedot(parametrit.at(0));
       }
       break;
    }
      
    case OMA:{
       pelaajat.front()->tulostaItselle();
       break;
    }
      
    case KARTTA:{
       const_itPelaaja pelaaja = ++pelaajat.begin();
       (*pelaaja)->tulostaVastustajalle();
       break;
    }
      
    case POMMITA:{
       itPelaaja pelaaja = ++pelaajat.begin();
       int px = 0;
       int py = 0;
       // tarkistetaan ett� pommitus onnistuu
       if(yf::string2int(parametrit.at(0), px) 
          &&  yf::string2int(parametrit.at(1), py) && (*pelaaja)->pommita(px, py)){
       }
       // pommitus ep�onnistui -> jatketaan peli�
	   else{
              // Tyhj�ll� on jatketaanko JATKA, joten sopii tilanteeseen
              komento = TYHJA;
	   }
       break;
    }
      
    case VAKOILE:{
       
       itPelaaja pelaaja = ++pelaajat.begin();
       
       int px = 0;
       int py = 0;
       yf::string2int(parametrit.at(0), px);
       yf::string2int(parametrit.at(1), py);
       
       // Jos vakoilu ei onnistu niin jatketaan peli� ilman vuoronvaihtoa
       if(!((*pelaaja)->vakoile(px, py))){
          // Tyhj�ll� on jatketaanko JATKA, joten sopii tilanteeseen
          komento = TYHJA;
       }
       break;
    }
      
    case KOMENTOKETJU:{
       
       pelaajat.front()->tulostaKomentoketju(parametrit.at(0));
       break;
    }
      
    case TILASTOT:{
       
       yf::tulostaTilastot(kierros, kaikkiPelaajat, false);
       
       break;
    }      
    case PAUSE:{
       if(!pause(parametrit.at(0))){
          komento = LOPETA;
	    }
       break;
    }
      
    case APUA:
    case HELP:{
       tulostaAputeksti();
       break;
    }
      
      //muissa tapauksissa toimitaan kuten haluttu...
    default:{
       break;
    }
   }
   
   return KOMENNOT[komento].jatketaanko;
}

// Funktio: void tarkistaParametrit(Kasky& komento, 
//          vector<string>& parametrit);
// Parametrit: komento, komennon parametrit
// Toiminta: tarkistaa ovatko komennolle annetut parametrit laillisia
// Paluuarvo: -
void koke::tarkistaParametrit(Kasky& komento, vector<string>& parametrit){
   
   // Jos kyseess� on komentovirhe tai tyhj� rivi niin ei tehd� mit��n
   if(komento == TYHJA || komento == KVIRHE){}
   // muissa tapauksissa tarkistetaan parametrit ja sen mukaan 
   // s��det��n komennon arvoa
   else{
      if(parametrit.size() > KOMENNOT[komento].max_parametreja){
         komento = LPVIRHE;
      }
      else if(parametrit.size() < KOMENNOT[komento].min_parametreja){
         komento = LVVIRHE;
      }
      
      
      //Tarkistetaan parametrien tyypin oikeellisuus
      
      // Jos kyseess� on integer niin tarkistetaan kaikkien parametrien
      // muunnosmahdollisuus
      if(KOMENNOT[komento].tyyppi == I){
         int temppi = 0;
         for(unsigned int i = 0; i < parametrit.size(); ++i){
            if(!yf::string2int(parametrit.at(i),temppi)){
               
               // Virheen tulostuksen joutuu t�ss� tapauksessa 
               // tulostamaan t��ll�.
               cerr << VIRHE << parametrit.at(i) << 
                 KOKLUKUPVIRHE << endl;
               komento = KLPVIRHE;
               break;
            }
         }
      }
      
      
   }
   
   
}

// Funktio: Kasky tarkistaKomento(string& komento);
// Parametrit: komento stringin�
// Toiminta: tarkistaa l�ytyyk� annettua komentoa
// Paluuarvo: annetun komennon enumeraatio jos moinen l�ytyy
koke::Kasky koke::tarkistaKomento(string& komento){ 
   Kasky knro = TYHJA;
   int kmaara = 0;
   yf::pieniksi(komento);
   
   //K�yd��n kaikki komennot l�vitse
   for(int i = 0; i < KOMENTOJA; ++i){
      if(KOMENNOT[i].nimi.find(komento) == 0){
         //tallennetaan l�ydettyjen sy�tteiden m��r� ja komennon numero
         ++kmaara;
         knro = KOMENNOT[ i ].komento;
      }
   }
   
   // jos komentoja l�ytyy liikaakin niin palautetaan virhe
   if(kmaara != 1){
      return KVIRHE;
   }
   else if(kmaara == 0){
      return TYHJA;
   }
   
   
   return knro;
}

// Funktio: void tulostaAputeksti();
// Parametrit: -
// Toiminta: tulostaa aputekstin komentokehoitteelle cout-virtaan
// Paluuarvo: -
void koke::tulostaAputeksti(){
   cout << "Komentorivin komennot:" << endl;
   cout << "TIEDOT <merkkijono>: N�ytt�� tiedot laivoista joissa esiintyy <merkkijono>" << endl;
   cout << "OMA: Oman kartan tulostus." << endl;
   cout << "KARTTA: Vastustajan kartan tulostus." << endl;
   cout << "POMMITA <x> <y>: Pommita ruutua x,y." << endl;
   cout << "VAKOILE <x> <y>: Vakoile ruutua x,y." << endl;
   cout << "KOMENTOKETJU <laiva>: Tulostetaan <laiva>:n alaiset." << endl;
   cout << "TILASTOT: Tulostetaan tilastot." << endl;
   cout << "APUA: T�m� aputuloste." << endl;
   cout << "HELP: T�m� aputuloste." << endl;
   cout << "PAUSE salasana: Pausettaa pelin ja lukitsee sen " 
     << "annetulla salasanalla." << endl;
   cout << "LOPETA: Lopettaa laivanupotuksen suorituksen." << endl;
   cout << endl;
}

// Funktio: bool pause(const string& salasana);
// Parametrit: salasana
// Toiminta: pausettaa pelin PAUSE-komennon m��ritt�m�ll� tavalla
// Paluuarvo: sy�tteen jatkuminen
bool koke::pause(const string& salasana){
   
   // rivinvaihdot
   for(unsigned int i = 0; i < 24; ++i){
      cout << endl;
   }
   
   cout << "Peli pausella. Avaa antamalla salasana: ";
   
   string temppi;
   
   // py�ritet��n t�t� kunnes tulee oikea salasana tai sy�te loppuu	
   while(true){
      string vertailtava;
      
      // haetaan salasana
      if(getline(cin, temppi)){
         istringstream striimi(temppi);
         striimi >> vertailtava;
         // tarkastetaan t�sm��k� salasana
         if(vertailtava == salasana){
            return true;
         }
         // Jos ei t�sm��, ilmoitus ja uudelleenpy�ritys
         else{
            cout << "V��r� salasana. Avaa antamalla salasana: ";
         }
      }
      else{
         // sy�te loppui
         return false;
      }
      
	}
   
   // T�nne ei pit�isi p��st�
   return false;
}

