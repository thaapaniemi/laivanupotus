#199006 Tomi Haapaniemi
#PT3 Makefile mallia 0.3

CC = tutg++
OBJS = main.o yleisfunktiot.o komentokehoite.o pelaaja.o laiva.o
BIN = laivanupotus

$(BIN): $(OBJS)
	$(CC) $(OBJS) -o $(BIN)

main.o: main.cc komentokehoite.cc yleisfunktiot.hh yleisfunktiot.cc komentokehoite.hh pelaaja.cc pelaaja.hh laiva.cc laiva.hh
	$(CC) -c main.cc

yleisfunktiot.o: yleisfunktiot.cc yleisfunktiot.hh pelaaja.cc pelaaja.hh
	$(CC) -c yleisfunktiot.cc

komentokehoite.o: komentokehoite.cc komentokehoite.hh yleisfunktiot.hh yleisfunktiot.cc pelaaja.cc pelaaja.hh laiva.cc laiva.hh
	$(CC) -c komentokehoite.cc
	
pelaaja.o: pelaaja.cc pelaaja.hh laiva.cc laiva.hh yleisfunktiot.hh yleisfunktiot.cc
	$(CC) -c pelaaja.cc
	
laiva.o: laiva.cc laiva.hh
	$(CC) -c laiva.cc
	
clean:
	rm -f $(BIN) *.o *~ core
	
vaihe1: main.cc
	$(CC) main.cc -o $(BIN)

vaihe2: $(OBJS)
	$(CC) $(OBJS) -o $(BIN)
	
vaihe3: $(OBJS)
	$(CC) $(OBJS) -o $(BIN)

vaihe4: $(OBJS)
	$(CC) $(OBJS) -o $(BIN)

