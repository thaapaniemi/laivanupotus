//*********************************************************************
// 199006 Tomi Haapaniemi
// haapani8
// 
// Ohjelmointi 2e
// Harjoitusty�
// 
// Laiva-luokka
//*********************************************************************

#include "laiva.hh"
#include<string>
#include<vector>
#include<map>
#include<iostream>


// Funktio: Laiva();
// Parametrit: -
// Toiminta: oletusrakentaja
// Paluuarvo: -
Laiva::Laiva():nimi_(""), kesto_(0), jaljella_(0),koordinaatit_(0,0,0,0){}

// Funktio: Laiva(const string& nimi, const Koordinaatit& koordinaatit)
// Parametrit: Laivan nimi ja koordinaatit
// Toiminta: Rakentaja, joka luo uuden laiva-olion ja m��rittelee sille nimen 
//           ja sijainnin
// Paluuarvo: -
Laiva::Laiva(const string& nimi, const Koordinaatit& koordinaatit):nimi_(nimi),
kesto_(0), jaljella_(0),koordinaatit_(koordinaatit){
   if(koordinaatit_.kx > koordinaatit_.px){
      kesto_ = koordinaatit_.kx - koordinaatit_.px + 1;
   }
   else if(koordinaatit_.kx < koordinaatit_.px){
      kesto_ = koordinaatit_.px - koordinaatit_.kx + 1;
   }
   else if(koordinaatit_.ky > koordinaatit_.py){
      kesto_ = koordinaatit_.ky - koordinaatit_.py + 1;
   }
   else if(koordinaatit_.ky < koordinaatit_.py){
      kesto_ = koordinaatit_.py - koordinaatit_.ky + 1;
   }
   else if(koordinaatit_.kx == koordinaatit_.px && 
           koordinaatit_.ky == koordinaatit_.py){
      kesto_ = 1;
   }
   
   jaljella_ = kesto_;
}

// Funktio: ~Laiva()
// Toiminta: Laiva-luokan purkaja
Laiva::~Laiva(){
   // T�m� luokka ei varaa muistia dynaamiseesti joten purkaja on tyhj�...
}

// Funktio: string laivanNimi() const;
// Parametrit: -
// Toiminta: -
// Paluuarvo: Laivan nimi
string Laiva::laivanNimi() const{
   return nimi_;
}

// Funktio: Koordinaatit sijainti() const;
// Parametrit: -
// Toiminta: -
// Paluuarvo: Laivan sijainti Koordinaatit-structissa
Laiva::Koordinaatit Laiva::sijainti() const{
   return koordinaatit_;
}


// Funktio: unsigned int jaljella() const;
// Parametrit: -
// Toiminta: -
// Paluuarvo: palauttaa ehjien ruutujen lukum��r�n
unsigned int Laiva::jaljella() const{
   return jaljella_;
}

// Funktio: unsigned int pituus() const;
// Parametrit: -
// Toiminta: -
// Paluuarvo: palauttaa laivan kokonaispituuden
unsigned int Laiva::pituus() const{
   return kesto_;
}

// Funktio: bool pommita(unsigned int x, unsigned int y);
// Parametrit: x ja y -koordinaatit
// Toiminta: varmistaa ett� laiva on kyseisess� koordinaatissa ja v�hent�� 
//           laivan kestoa
// Paluuarvo: onnistuiko pommitus
bool Laiva::pommita(unsigned int x, unsigned int y){
   bool onkoX = false;
   bool onkoY = false;
   
   // Kun x on laivan x-akselistolla
   if(koordinaatit_.kx >= x && koordinaatit_.px <= x){
      onkoX = true;
   }
   else if(koordinaatit_.kx <= x && koordinaatit_.px >= x){
      onkoX = true;
   }
   
   // Kun y on laivan y-akselistolla
   if(koordinaatit_.ky >= y && koordinaatit_.py <= y){
      onkoY = true;
   }
   else if(koordinaatit_.ky <= y && koordinaatit_.py >= y){
      onkoY = true;
   }
   
   // Pommitus onnistui t�lle laivalle jos ehdot t�yttyv�t
   if(onkoX && onkoY){
      --jaljella_;
      return true;
   }
   
   return false;
   
}

// Funktio: bool lisaaAlainen(Laiva* alainen);
// Parametrit: pointteri lis�tt�v��n laivaan
// Toiminta: lis�� pointtterin p��ss� olevan laivan alaiseksi
// Paluuarvo: onnistuiko lis�ys
bool Laiva::lisaaAlainen(Laiva* alainen){
   // Tarkistetaan ettei lis�tt�v�� alaista ole jo olemassa
   if(alaiset_.count(alainen->laivanNimi()) == 0){
      // Lis�t��n alainen mappiin
      alaiset_.insert(make_pair(alainen->laivanNimi(), alainen));
      return true;
   }
   
   return false;
}

// Funktio:    void tulostaAlaiset() const;
// Parametrit: -
// Toiminta: tulostaa laivan alaiset cout-virtaan (tiedot-komentoa varten)
// Paluuarvo: -
void Laiva::tulostaAlaiset() const{
   for(map<string, Laiva*>::const_iterator i = alaiset_.begin(); 
       i != alaiset_.end(); ++i){
      cout << i->first << endl;
   }
}

// Funktio: void tulostaKomentoketju() const;
// Parametrit: -
// Toiminta: kutsuu tulostaKomentoketjua(mones) 1:ll�
// Paluuarvo: -
void Laiva::tulostaKomentoketju() const{
   unsigned int mones = 1;
   tulostaKomentoketju(mones);
}

// Funktio: void tulostaKomentoketju(unsigned int mones) const;
// Parametrit: mones alainen
// Toiminta: tulostaa laivan nimen ja 3x monennen verran tyhj��, sek� kutsuu 
// rekursiivisesti itse��n alamaisille
// Paluuarvo: -
void Laiva::tulostaKomentoketju(unsigned int mones) const{
   // tulosta laivan nimi
   cout << laivanNimi() << " ("<< koordinaatit_.kx << "," << koordinaatit_.ky
     << ")" << endl;
   
   // suoritetaan alaisten komentoketjun tulostus
   for(map<string, Laiva*>::const_iterator i = alaiset_.begin(); 
       i != alaiset_.end(); ++i){
      
      // laitetaan riitt�v� m��r� v�lej� nimen alkuun
      for(unsigned int n = 0; n < mones; ++n){
         cout << "   ";
      }
      
      // itse tulostus
      i->second->tulostaKomentoketju(mones + 1);
   }
}

