//*********************************************************************
// 199006 Tomi Haapaniemi
// haapani8
// 
// Ohjelmointi 2e
// Harjoitusty�
// 
// Yleisfunktiot-moduuli
// Sis�lt�� mainin ja komentokehoitteen k�ytt�mi� enempi-v�hempi 
// yleishy�dyllisi� funktioita
//*********************************************************************

#include<string>
#include<list>
#include<sstream>
#include "pelaaja.hh"
#include "yleisfunktiot.hh"

using namespace std;

bool yf::string2int(string& stringi, unsigned int& numero){
   int temppi = 0;
   istringstream virta(stringi);
   if(virta >> temppi && temppi >= 0){
      numero = temppi;
      return true;
   }
   return false;
}

bool yf::string2int(string& stringi, int& numero){
   int temppi = 0;
   istringstream virta(stringi);
   if(virta >> temppi){
      numero = temppi;
      return true;
   }
   return false;
}

void yf::pieniksi( string& mjono ){
   for( unsigned int i = 0; i < mjono.length(); ++i ) {
      // ��kk�set
      if(mjono.at(i) == '�'){
         mjono.at(i) = '�';
      }
	  // ��kk�set
      else if(mjono.at(i) == '�'){
         mjono.at(i) = '�';
      }
      // Laitetaan viel� ��kk�setkin
      else if(mjono.at(i) == '�'){
         mjono.at(i) = '�';
      }
	  // ja ��kkoset
	  else if(mjono.at(i) == '�'){
         mjono.at(i) = '�';
      }
      else{
         mjono.at( i ) = tolower( mjono.at( i ) );
      }
   }
}

void yf::tulostaTilastot(unsigned int kierros, list<Pelaaja*>& pelaajat, 
                     bool loppuiko){
   cout << "Pelikierros: " << kierros << endl;
   
   // Haetaan voittaja
   Pelaaja* voittaja = 0;
   bool tasapeli = false;
   
   // Tulostetaan tiedot ja tallennetaan lista pelaajista voiton selvittely�
   // varten
   for(list<Pelaaja*>::const_iterator i = pelaajat.begin(); 
       i != pelaajat.end(); ++i){
      Pelaaja * temppi = *i;
      
      // Tulostetaan tilastotieto
      temppi->tulostaTilasto();
      
      // alustetaan voittaja
      if(voittaja == 0){
         voittaja = *i;
      }
      // tarkastetaan voittaja
      else if(temppi->ehjia() > voittaja->ehjia() ){
         voittaja = temppi;
         tasapeli = false;
      }
      else if(temppi->ehjia() == voittaja->ehjia()){
         tasapeli = true;
      }
      
   }
   
   // Tulostetaan voittaja
   if(tasapeli){
      // tasapeli
      cout << "Tasapeli" << endl;
   }
   else if(loppuiko == false){
      cout << "Voitolla " << voittaja->nimi() << endl;
   }
   else{
      // muussa tapauksessa voittaja on yksiselitteinen
      cout << "Voittaja " << voittaja->nimi() << endl;
   }
}
