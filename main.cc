//*********************************************************************
// 199006 Tomi Haapaniemi
// haapani8
// 
// Ohjelmointi 2e
// Harjoitusty�
// 
// Main-moduuli
//*********************************************************************

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <list>
#include "komentokehoite.hh"
#include "pelaaja.hh"
#include "laiva.hh"
#include "yleisfunktiot.hh"

using namespace std;

const string MAINPVIRHE = "Virhe: Virheelliset komentoriviparametrit.";
const string KEHOITE =" > ";

// Ohjelman mahdolliset toimenpiteet tarksituksissa(jatketaan, E_S, tai E_F)
enum  Toimenpide {
   OK, HELPPI, VIRHE
};

// Funktio: Toimenpide tarkistaParametrit(vector<string>& parametrit,  
//          vector<string>& laivatiedostot,  unsigned int& koko);
// Parametrit: k�ynnistysparametrit, laivatiedostojen nimet, kartan koko
// Toiminta:  tarkistaa ett� parametrit ovat oikean muotoiset ja sijoittaa
//            pelaajien tiedostot laivatiedostoihin ja kartan koon kokoon
// Paluuarvo: Miten jatketaan (EXIT SUCCESS/FAILURE, helpin tulostus tai 
//            jatkuu)
Toimenpide tarkistaParametrit(list<string>& parametrit, 
                              vector<string>& laivatiedostot, 
                              unsigned int& koko);

// Funktio: void tulostaOhjeteksti();
// Parametrit: -
// Toiminta: tulostaa k�ynnistyksen ohjetekstin cout-virtaan
// Paluuarvo: -
void tulostaOhjeteksti();

// Funktio: bool lueLaivat(string& tnimi, list<Pelaaja*>& pelaajat, 
//          const unsigned int& koko);
// Parametrit: tiedoston nimi, pelaajalista, kartan koko
// Toiminta: luo pelaajan ja lukee laivatiedostosta laivat pelaajaan
// Paluuarvo: onnistuiko luku
bool lueLaivat(string& tnimi, list<Pelaaja*>& pelaajat, 
               const unsigned int& koko);

// Funktio: void pyoritaKomentokehoitetta(list<Pelaaja*>& pelaajat);
// Parametrit: pelaajalista
// Toiminta: py�ritt�� komentokehoitetta
// Paluuarvo: -
void pyoritaKomentokehoitetta(list<Pelaaja*>& pelaajat);

// Funktio: int main(int argc, char* argv[]){
// Parametrit: k�ynnistysparametrit
// Toiminta: suorittaa lukemiset ja alkaa py�ritt�� komentokehoitetta
// Paluuarvo: EXIT_SUCCESS /EXIT_FAILURE
int main(int argc, char* argv[]){
   list<string> krparametrit(argv, argv + argc);
   vector<string> laivatiedostot;
   unsigned int kartanKoko = 0;
   
   // Tulostetaan t�m� vaikeasti yleisiin vaatimuksiin piilotettu vaatimus
   cout << "### OHJ-1151 Ohjelmointi IIe" << endl;
   cout << "### Tomi Haapaniemi, 199006, haapani8" << endl;
   
   
   
   Toimenpide jatketaanko = tarkistaParametrit(krparametrit, 
                                               laivatiedostot, kartanKoko);
   
   // Suoritetaan tarkastuksen virhekoodit
   if(jatketaanko == HELPPI){
      tulostaOhjeteksti();
      return EXIT_SUCCESS;
   }
   else if(jatketaanko == VIRHE){
      cerr << MAINPVIRHE << endl;
      tulostaOhjeteksti();
      return EXIT_FAILURE;
   }
   
   // Pelaajat:
   list<Pelaaja*> pelaajat;
   
   for(unsigned int i = 0; i < laivatiedostot.size(); ++i){
      
      
      if(!lueLaivat(laivatiedostot.at(i), pelaajat, kartanKoko)){
         while(!pelaajat.empty()){
            delete pelaajat.back();
            pelaajat.back() = 0;
            pelaajat.pop_back();
         }
         return EXIT_FAILURE;
      }
   }
   
   // Tarkistetaan ett� onko pelaajilla eri m��r� laivaruutuja
   for(list<Pelaaja*>::const_iterator i = pelaajat.begin(); 
       i != pelaajat.end(); ++i){
      Pelaaja* temppi = *i;
      if(temppi->ruutuja() != pelaajat.front()->ruutuja()){
         cout << "Pelaajilla on eri m��r� laivaruutuja." << endl;
         break;
      }
   }
   
   // K�ynnistet��n komentokehoitteen py�ritysfunktio
   pyoritaKomentokehoitetta(pelaajat);
   
         
   while(!pelaajat.empty()){
      delete pelaajat.back();
      pelaajat.back() = 0;
      pelaajat.pop_back();
   }
   
   return EXIT_SUCCESS;
}

// Funktio: Toimenpide tarkistaParametrit(vector<string>& parametrit,  
//          vector<string>& laivatiedostot,  unsigned int& koko);
// Parametrit: k�ynnistysparametrit, laivatiedostojen nimet, kartan koko
// Toiminta:  tarkistaa ett� parametrit ovat oikean muotoiset ja sijoittaa 
//            pelaajien tiedostot laivatiedostoihin ja kartan koon kokoon
// Paluuarvo: Miten jatketaan (jatkuu, virhe, helpin tulostus)
Toimenpide tarkistaParametrit(list<string>& parametrit, 
                              vector<string>& laivatiedostot, 
                              unsigned int& koko){
   
   bool helppilippu = false;
   bool kokolippu = false;
   bool virhelippu = false;
   unsigned int monespelaaja = 1;
   
   // poistetaan ohjelman nimi listasta
   parametrit.pop_front();
   
   // k�yd��n l�pi kaikki parametrit
   while(parametrit.size() > 0){
      
      string verrattava = parametrit.front();
      
      // sy�tteen pituus yli merkin joten siin� voi olla jotain j�rkev��
      if(verrattava.size() > 1){
         verrattava = verrattava.substr(1);
         parametrit.pop_front();
      }
      // sy�tteen pituus oli tasan merkki joten siit� ei voida lukea mit��n
      // j�rkev��
      else{
         virhelippu = true;
         parametrit.pop_front();
         continue;
         
      }
      
      if(verrattava == "k"){
         // tarkistetaan ettei kokoa ole jo annettu
		 if (kokolippu == true){
			virhelippu = true;
		 }
		 
		 kokolippu = true;
         
		 // onnistunut koon luku
         if(parametrit.size() > 0 && yf::string2int(parametrit.front(),koko)){
            parametrit.pop_front();
         }
         // ep�onnistunut vaikka parametrej� oli tarpeeksi
         else if(parametrit.size() > 0){
            parametrit.pop_front();
            virhelippu = true;
         }
         // ep�onnistui koska parametrej� ei ollut tarpeeksi
         else{
            virhelippu = true;
         }
         
      }
      // testataan l�ytyyk� helppi
      else if(verrattava == "h" || verrattava == "-help"){
         helppilippu = true;
      }
      else{
         unsigned int numero = 0;
         // jos pelaaja l�ytyy j�rjestyksess� niin lis�t��n se vektoriin
         if(yf::string2int(verrattava, numero) && numero == monespelaaja 
            && parametrit.size() > 0){
            laivatiedostot.push_back(parametrit.front());
            parametrit.pop_front();
            ++monespelaaja;
         }
         // jos pelaaja on v��r� pelaaja j�rjestyksess�
         else if(numero != 0 && numero != monespelaaja){
            virhelippu = true;
            parametrit.pop_front();
         }
         // jos pelaaja on oikea mutta parametrej� ei riit�
         else if(numero == monespelaaja && parametrit.size() == 0){
            virhelippu = true;
         }
         // muussa tapauksessa parametri� ei l�ydy
         else if (parametrit.size() != 0){
            virhelippu = true;
         }
         else{
            virhelippu = true;
         }
      }
      
   }
	
   // jos pelaajien m��r� on pienempi kuin 2
   if(laivatiedostot.size() < 2){
      virhelippu = true;
   }
   
   // jos helppilippu asettui todeksi
   if(helppilippu == true){
      return HELPPI;
   }
   // jos kokolippu ei asettunut todeksi tai virhelippu asettui todeksi
   else if(kokolippu != true || virhelippu == true){
      return VIRHE;
   }
   // muussa tapauksessa parametrit kelpasivat
   else{
      return OK;
   }
   
   return VIRHE;
}

// Funktio: void tulostaOhjeteksti();
// Parametrit: -
// Toiminta: tulostaa k�ynnistyksen ohjetekstin cout-virtaan
// Paluuarvo: -
void tulostaOhjeteksti(){
   
   cout << "-<numero> <laivatiedosto> Pelaajan <numero> laivatiedoston nimi."
     << endl << "     Annetaan nrosta 1 alkaen kasvaen aina yhdell�." << endl;
   cout << "-k <kent�n koko> Kent�n sivun pituus kokonaislukuna." << endl;
   cout << "-h tai --HELPPI HELPPIpiteksti" << endl;
   
}

// Funktio: bool lueLaivat(string& tnimi, list<Pelaaja*>& pelaajat, 
//          const unsigned int& koko);
// Parametrit: tiedoston nimi, pelaajalista, kartan koko
// Toiminta: luo pelaajan ja lukee laivatiedostosta laivat pelaajaan
// Paluuarvo: onnistuiko luku
bool lueLaivat(string& tnimi, list<Pelaaja*>& pelaajat, 
               const unsigned int& koko){
   
   // Luodaan mappi johon tallennetaan laivat
   map<string, Laiva*> laivat;
   
   // Avataan tiedosto
   ifstream tiedosto(tnimi.c_str());
   if(tiedosto){
      
      bool virhe = false;
      
      // Luodaan temppistringi
      string temppi;
      
      getline(tiedosto, temppi);
      pelaajat.push_back (new Pelaaja(temppi, koko));
      
      // Luetaan laivatiedostoa rivi kerrallaan
      while(getline(tiedosto, temppi)){
         
         string laivannimi;
         string loput;
         unsigned int keulanX = 0;
         unsigned int keulanY = 0;
         
         unsigned int peranX = 0;
         unsigned int peranY = 0;
         
         // Kun saavutetaan tyhj� rivi niin keskeytet��n
         // laivojen luku
         if(temppi.size() == 0){
            break;
         }
         
         // luodaan stringist� streami
         istringstream striimi(temppi);
         
         // Luetaan laivan tiedot rivilt� (eli streamista)
         if(striimi >> laivannimi && striimi >> keulanX && striimi >> keulanY
            && striimi >> peranX && striimi >> peranY && !(striimi >> loput)){
            // Tarkastetaa�n ett� laivat eiv�t ole vinottain
            if(!(keulanX == peranX || keulanY == peranY)){
               virhe = true;
            }
            
            if(keulanX >= koko ||  keulanY >= koko || peranX >= koko 
               || peranY >= koko){
               
               virhe = true;
            }
            
            
            //Toimintaa
            // Luodaan laivaolio
            Laiva* temppilaiva = new Laiva 
              (laivannimi, Laiva::Koordinaatit(keulanX, keulanY, 
                                               peranX, peranY));
            
            // Lis�t��n laiva karttaan ja varmistetaan ett� lis�ys onnistui
            if(!(pelaajat.back()->lisaaLaiva(temppilaiva))){
               // T�ss� tapauksessa laivan lis��minen ei onnistu koska joko 
               // laivat ovat p��llekk�in tai jotain muuta ei-toivottua
               virhe = true;
               delete temppilaiva;
               temppilaiva = 0;
               
            }
            // T�ss� tapauksessa lis�ys onnistui, joten lis�t��n laiva my�s 
            // mappiimme
            else{
               yf::pieniksi(laivannimi);
               
               // Tarkastetaan onko samanniminen laiva jo olemassa
               if(laivat.count(laivannimi) != 0){
                  virhe = true;
				  delete temppilaiva;
                  temppilaiva = 0;
               }
               
               laivat.insert(make_pair(laivannimi, temppilaiva));
			   
            }
            
         } // Koordinaatteja ei saatu luettua joten virhe
         else{
            virhe = true;
            
         }
         
         // Virhetilanteessa tulostetaan virhe
         if(virhe == true  || laivat.size() == 0){
            
            cerr << "Virhe: Virheellinen tiedosto " << tnimi << "." << endl;
            return false;
         }
         
      }
      
      
      // temppimuuttujat komentoketjun laivoille
      string laiva1;
      string laiva2;
      string loput;
      
      // tyhj�t v�lit pois
      while(getline(tiedosto, temppi) && temppi.size() == 0);
      
      // Luetaan komentoketjut
      do{		
         if(temppi.size() == 0){
            // Tiedoston luku ep�onnistui ja tiedostoon j�i viel� tavaraa
            if(tiedosto >> loput){
               virhe = true;
            }
            break;}
         // luodaan stringist� streami
         istringstream striimi(temppi);
         
         // Luetaan komentoketjut v�lill� laiva1 -> laiva2
         if(striimi >> laiva1 && striimi >> laiva2 && !(striimi >> loput)){
            
            // tuhotaan merkkiriippuvaisuus
            yf::pieniksi(laiva1);
            yf::pieniksi(laiva2);
            
            // Tarkastetaan ett� laivat l�ytyv�t
            if(laivat.count(laiva1) == 0 
               || laivat.count(laiva2) == 0){ // virhe
               
               virhe = true;
               break;
            }
            
            // lis�t��n laivan 1 komentoketjuun laiva 2
            else{
               if(!laivat[laiva1]->lisaaAlainen(laivat[laiva2])){
                  virhe = true;
                  cerr << "440" << endl;
                  break;
               }
            }
         }
         // striimist� ei saatu tarvittavia tietoja joten virhe
         else{
            virhe = true;
            break;
         }
      }while(getline(tiedosto, temppi));
      
      tiedosto.close();
      
      // Tulostetaan virhe
      if(virhe == true || laivat.size() == 0){
         cerr << "Virhe: Virheellinen tiedosto " << tnimi << "." << endl;
         return false;
      }
      
      return true;
   }
   
   // Tiedoston luku ep�onnistui joten tulostetaan virhe
   else{
      cerr << "Virhe: Tiedostoa " << tnimi << " ei voida lukea." << endl;
   }
   
   return false;
}

// Funktio: void pyoritaKomentokehoitetta(list<Pelaaja*>& pelaajat);
// Parametrit: pelaajalista
// Toiminta: py�ritt�� komentokehoitetta
// Paluuarvo: -
void pyoritaKomentokehoitetta(list<Pelaaja*>& pelaajat){
   //Luodaan komentorivin komento ja parametrit -s�ili�t
   vector<string> parametrit;
   koke::Kasky komento = koke::TYHJA;
   koke::Palautus jatko = koke::LOPPU;
   unsigned int kierros = 1;
   
   // Luodaan pelikierrokselle uusi lista
   list<Pelaaja*> pelissaOlevat(pelaajat);
   // Ja h�vinneille oma lista
   //list<Pelaaja*> havinneet;
   
   Pelaaja* ensimmainen = pelissaOlevat.front();
   
   
   // Suoritetaan komentokehoite
   while(true){
      do{
         // tyhjennet��n komentokehoitteen komento ja parametrit
         parametrit.clear();
         komento = koke::TYHJA;
         
         // Tulostetaan komentokehoite
         cout << pelissaOlevat.front()->nimi() << KEHOITE;
         
         // luetaan komento
         if(!koke::lueKomento(komento, parametrit)){
            jatko = koke::LOPPU;
            break;
         }
         
         // suoritetaan komento
         jatko = koke::suoritaKomento(kierros, pelissaOlevat, pelaajat, komento,
                                parametrit);
         
         // jatketaan niin kauan kunnes pit�� vaihtaa pelaajaa
      }while(jatko == koke::JATKA);
      
      // vaihdetaan pelaaja
      if(jatko == koke::VAIHDA){
         pelissaOlevat.push_back(pelissaOlevat.front());
         pelissaOlevat.pop_front();
         
         // T�h�n v�liin h�vinneisyyden tarkastelu
         if(pelissaOlevat.front()->ehjia() == 0){
            // Jos pelikierroksen aloittaja poistuu niin vaihdetaan 
            // aloittajaksi seuraava henkil�
            if(ensimmainen == pelissaOlevat.front()){
               pelissaOlevat.pop_front();
               ensimmainen = pelissaOlevat.front();
            }
            else{
               pelissaOlevat.pop_front();
            }
         }
         
         // Tarkastetaan ett� pelaajia on viel� enemm�n kuin 1 kpl
         if(pelissaOlevat.size() > 1){
            cout << "Vuoro p��ttyi. Seuraavana vuorossa " 
              << pelissaOlevat.front()->nimi() << "." << endl;
            string temppi;
            getline(cin, temppi);
            if(pelissaOlevat.front() == ensimmainen){
               ++kierros;
            }
            
            // tulostetaan 24 rivinvaihtoa
            for(unsigned int i = 0; i < 24; ++i){
               cout << endl;
            }
            
         }
         // jos ei ole niin lopetetaan
         else{
            jatko = koke::LOPPU;
         }
      }
      
      // Lopetetaan peli
      if(jatko == koke::LOPPU){
         
         //Tulostetaan loppuinformaatiot
         cout << "Peli p��ttyi!" << endl;
         
         // Suoritetaan tietojen tulostus
         yf::tulostaTilastot(kierros, pelaajat, true);
			
         return;
      }
      
   }
}
