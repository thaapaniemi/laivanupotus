//*********************************************************************
// 199006 Tomi Haapaniemi
// haapani8
// 
// Ohjelmointi 2e
// Harjoitusty�
// 
// Laiva-luokka
//*********************************************************************

#ifndef LAIVA_HH
#define LAIVA_HH

#include<string>
#include<vector>
#include<map>
#include<iostream>

using namespace std;

class Laiva{
   
 public:
   
   // Funktio: Laiva();
   // Parametrit: -
   // Toiminta: oletusrakentaja
   // Paluuarvo: -
   Laiva();
   
   // Funktio: ~Laiva();
   // Parametrit: -
   // Toiminta: purkaja
   // Paluuarvo: -
   ~Laiva();
   
   // Laiva-luokan koordinaatit m��rittelev� struct
   struct Koordinaatit{
      unsigned int kx; // keulan koordinaatit
      unsigned int ky;
      unsigned int px; // per�n koordinaatit
      unsigned int py;
      
      Koordinaatit(){};
      Koordinaatit(unsigned int a,unsigned int b,unsigned int c,
                   unsigned int d):kx(a),ky(b),px(c),py(d){};
   };
   
   // Funktio: Laiva(const string& nimi, const Koordinaatit& koordinaatit);
   // Parametrit: Laivan nimi ja koordinaatit
   // Toiminta: Rakentaja, joka luo uuden laiva-olion ja m��rittelee sille 
   //           nimen ja sijainnin
   // Paluuarvo: -
   Laiva(const string& nimi, const Koordinaatit& koordinaatit);
   
   // Funktio: string laivanNimi() const;
   // Parametrit: -
   // Toiminta: -
   // Paluuarvo: Laivan nimi
   string laivanNimi() const;
   
   
   // Funktio: Koordinaatit sijainti() const;
   // Parametrit: -
   // Toiminta: -
   // Paluuarvo: Laivan sijainti Koordinaatit-structissa
   Koordinaatit sijainti() const;
   
   // Funktio:    void tulostaAlaiset() const;
   // Parametrit: -
   // Toiminta: tulostaa laivan alaiset cout-virtaan (tiedot-komentoa varten)
   // Paluuarvo: -
   void tulostaAlaiset() const;
   
   // Funktio: void tulostaKomentoketju(unsigned int mones) const;
   // Parametrit: mones alainen
   // Toiminta: tulostaa laivan nimen ja 3x monennen verran tyhj��, sek� 
   //           kutsuu rekursiivisesti itse��n alamaisille
   // Paluuarvo: -
   void tulostaKomentoketju(unsigned int mones) const;
   
   // Funktio: void tulostaKomentoketju() const;
   // Parametrit: -
   // Toiminta: kutsuu tulostaKomentoketjua(mones) 1:ll�
   // Paluuarvo: -
   void tulostaKomentoketju() const;
   
   // Funktio: unsigned int jaljella() const;
   // Parametrit: -
   // Toiminta: -
   // Paluuarvo: palauttaa ehjien ruutujen lukum��r�n
   unsigned int jaljella() const;
   
   // Funktio: unsigned int pituus() const;
   // Parametrit: -
   // Toiminta: -
   // Paluuarvo: palauttaa laivan kokonaispituuden
   unsigned int pituus() const;
   
   
   // Funktio: bool pommita(unsigned int x, unsigned int y);
   // Parametrit: x ja y -koordinaatit
   // Toiminta: varmistaa ett� laiva on kyseisess� koordinaatissa ja v�hent�� 
   //           laivan kestoa
   // Paluuarvo: onnistuiko pommitus
   bool pommita(unsigned int x, unsigned int y);
   
   // Funktio: bool lisaaAlainen(Laiva* alainen);
   // Parametrit: pointteri lis�tt�v��n laivaan
   // Toiminta: lis�� pointtterin p��ss� olevan laivan alaiseksi
   // Paluuarvo: onnistuiko lis�ys
   bool lisaaAlainen(Laiva* alainen);
   
   
 private:
   
   string nimi_;
   unsigned int kesto_;
   unsigned int jaljella_;
   Koordinaatit koordinaatit_;
   
   // Laivan alaiset
   map<string, Laiva*> alaiset_;
   
   // Kopiorakentaja ja sijoitusoperaattori
   Laiva(const Laiva& vanha);
   Laiva& operator=(Laiva& sijoitettava);
   
};
#endif
