//*********************************************************************
// 199006 Tomi Haapaniemi
// haapani8
// 
// Ohjelmointi 2e
// Harjoitusty�
// 
// Pelaaja-luokka
// huolehtii pelaajan toiminnoista (laivat, pommitukset, kartat, yms)
//*********************************************************************

#ifndef PELAAJA_HH
#define PELAAJA_HH

#include<string>
#include <iostream>
#include <vector>
#include <map>
#include "laiva.hh"
#include "yleisfunktiot.hh"

class Pelaaja{
   
 public:
   
   // Funktio: Pelaaja();
   // Parametrit: -
   // Toiminta: Oletusrakentaja
   // Paluuarvo: -
   Pelaaja();
   
   // Funktio: ~Pelaaja();
   // Parametrit: -
   // Toiminta: Purkaja luokalle
   // Paluuarvo: -
   ~Pelaaja();
   
   // Funktio: Pelaaja(const string& unimi, const unsigned int& ukoko);
   // Parametrit: pelaajan nimi, kartan koko
   // Toiminta: Rakentaja, joka alustaa pelaajalle nimen ja kartan koon
   // Paluuarvo: -
   Pelaaja(const string& unimi, const unsigned int& ukoko);
   
   // Funktio: bool lisaaLaiva(Laiva* laiva);
   // Parametrit: pointteri lis�tt�v��n laivaan
   // Toiminta: Lis�� laivan pelaaja-olioon
   // Paluuarvo: onnistuiko lis�ys
   bool lisaaLaiva(Laiva* laiva);
   
   // Funktio: bool tulostaKomentoketju(string parametri) const;
   // Parametrit: laivan nimi
   // Toiminta: tulostaa cout-virtaan annetun laivan komentoketjun
   // Paluuarvo: l�ytyik� laivaa
   bool tulostaKomentoketju(string parametri) const;
   
   // Funktio: void tulostaItselle() const;
   // Parametrit: -
   // Toiminta: tulostaa OMA-komentoa vastaavan kartan cout-virtaan
   // Paluuarvo: -
   void tulostaItselle() const;
   
   // Funktio: void tulostaVastustajalle() const;
   // Parametrit: -
   // Toiminta: tulostaa KARTTA-komentoa vastaavan kartan cout-virtaan
   // Paluuarvo: -
   void tulostaVastustajalle() const;
   
   // Funktio: bool pommita(unsigned int& x, unsigned int& y);
   // Parametrit: pommitettavan ruudun x ja y-koordinaatit
   // Toiminta: pommitaa annettua ruutua
   // Paluuarvo: onnistuiko pommitus
   bool pommita(int& x, int& y);
   
   // Funktio: bool vakoile(unsigned int kx, unsigned int ky);
   // Parametrit: vakoiltavan ruudun x ja y-koordinaatit
   // Toiminta: vakoilee annettua ruutua
   // Paluuarvo: onnistuiko vakoilu
   bool vakoile(int& kx, int& ky);
   
   // Funktio: unsigned int tulostaTilasto() const;
   // Parametrit: -
   // Toiminta: tulostaa cout-virtaan pelaajan tilastotiedot
   // Paluuarvo: pelaajan ehjien ruutujen lukum��r�
   unsigned int tulostaTilasto() const;
   
   // Funktio: void tiedot() const;
   // Parametrit: -
   // Toiminta: tulostaa tiedot kaikista laivoista
   // Paluuarvo: -
   void tiedot() const;
   
   // Funktio: bool tiedot(const string& parametri) const;
   // Parametrit: laivan nimi tai sen osa
   // Toiminta: etsii laivan annetulla parametrilla ja jos laiva l�ytyy 
   //           tulostaa sen tiedot
   // Paluuarvo: l�ytyik� laiva
   bool tiedot(const string& parametri) const;
   
   // Funktio: unsigned int koko() const;
   // Parametrit: -
   // Toiminta: -
   // Paluuarvo: palauttaa kartan koon
   unsigned int koko() const;
   
   // Funktio: string nimi() const;
   // Parametrit: -
   // Toiminta: -
   // Paluuarvo: palauttaa pelaajan nimen
   string nimi() const;
   
   // Funktio: unsigned int ruutuja() const;
   // Parametrit: -
   // Toiminta: laskee pelaajan laivaruutujen kokonaism��r�n
   // Paluuarvo:  pelaajan laivaruutujen kokonaism��r�
   unsigned int ruutuja() const;
   
   // Funktio: unsigned int ehjia() const;
   // Parametrit: -
   // Toiminta: laskee pelaajan ehjien laivaruutujen m��r�n
   // Paluuarvo: pelaajan ehjien laivaruutujen m��r�
   unsigned int ehjia() const;
   
private:
   
   // Funktio: unsigned int laivojaJaljella() const;
   // Parametrit: -
   // Toiminta: laskee ei-uponneiden laivojen m��r�n
   // Paluuarvo: ei-uponneiden laivojen m��r�
   unsigned int laivojaJaljella_() const;
   
   
   // Funktio: void lisaaNumerointi_(char merkki, 
   //          vector<string>& tulostusvektori) const;
   // Parametrit: merkki jolla tulostusvektori t�ytet��n, tulostusvektori
   // Toiminta: luo tulostusvektorin jossa on koordinaatisto reunassa ja 
   // joka on muuten t�ytetty annetulla merkill�
   // Paluuarvo: -
   void lisaaNumerointi_(char merkki, vector<string>& tulostusvektori) const;
   
   // Pelaajan nimi
   string nimi_;
   
   // kartan koko
   unsigned int koko_;
   
   // Lista laivoista
   map<string, Laiva*> laivat_;

   // tieto vakoilusta
   bool vakoiltu_;
   
   // Kartan koordiinaattipisteet pommituksille
   struct Ruutu_{
      Laiva* laiva;
      bool pommitettu;
      
      // Rakentajat k�ytt�� helpottamaan
      Ruutu_(){};
      Ruutu_(Laiva* al, bool op):laiva(al),pommitettu(op){};
   };
   
   // Koordinaatisto_:n structi
   struct AvainKoordinaatti_{
      unsigned int x;
      unsigned int y;
      
      
      //AvainKoordinaatti_ - structille vertailuoperaattori mapin k�ytt�� varten
      bool operator ()(const Pelaaja::AvainKoordinaatti_& a1, 
                       const Pelaaja::AvainKoordinaatti_& a2) const;
      
      // Rakentajat structille k�ytt�� helpottamaan
      AvainKoordinaatti_();
      AvainKoordinaatti_(unsigned int ax, unsigned int ay);
   };
   
   // Funktio: bool lisaaKoordinaatti_(AvainKoordinaatti_ kpiste, 
   //          Ruutu_ ruutu);
   // Parametrit: koordinaattipistestructi, pisteen tiedot
   // Toiminta: lis�� uuden koordinaattiruudun karttaan
   // Paluuarvo: onnistuiko lis�ys
   bool lisaaKoordinaatti_(AvainKoordinaatti_ kpiste, Ruutu_ ruutu);
   
   // Pommitus- sek� laivapisteet
   map<AvainKoordinaatti_, Ruutu_, AvainKoordinaatti_> koordinaatisto_;
   
   // typedeffityst� iteraattorien k�ytt�� helpottamaan
   typedef map<string, Laiva*>::const_iterator const_itLaiva;
   typedef map<string, Laiva*>::iterator itLaiva;
   
   typedef map<AvainKoordinaatti_, Ruutu_, AvainKoordinaatti_>::iterator 
     itKoord;
   typedef map<AvainKoordinaatti_, Ruutu_, AvainKoordinaatti_>::const_iterator
     const_itKoord;
	 
   // Kopiorakentaja ja sijoitusoperaattori
   Pelaaja(const Pelaaja& vanha);
   Pelaaja& operator=(Pelaaja& sijoitettava);
};
#endif

