//*********************************************************************
// 199006 Tomi Haapaniemi
// haapani8
// 
// Ohjelmointi 2e
// Harjoitusty�
// 
// Yleisfunktiot-moduuli
// Sis�lt�� mainin ja komentokehoitteen k�ytt�mi� enempi-v�hempi 
// yleisk�ytt�isi� funktioita
//*********************************************************************

#ifndef YLEISFUNKTIOT_HH
#define YLEISFUNKTIOT_HH

#include<string>
#include<list>
#include<sstream>
#include "yleisfunktiot.hh"

// ristiinlinkityksen takia tehd��n n�in
class Pelaaja;

// Luodaan yleisfunktioille oma namespace, etteiv�t
// vahingossakaan sekoitu muualle
namespace yf{
   
   // Funktio: bool string2int(string& stringi, unsigned int& numero);
   // Parametrit: stringi joka muutetaan integeriksi, merkit�n integer johon sijoitetaan
   // Toiminta: muuttaa stringin merkitt�m�ksi integeriksi
   // Paluuarvo: onnistuiko muunnos
   bool string2int(string& stringi, unsigned int& numero);

   // Funktio: bool string2int(string& stringi, int& numero);
   // Parametrit: stringi joka muutetaan integeriksi, integer johon sijoitetaan
   // Toiminta: muuttaa stringin normi-integeriksi
   // Paluuarvo: onnistuiko muunnos   
   bool string2int(string& stringi, int& numero);
   
   // Funktio: void pieniksi( string& mjono );
   // Parametrit: merkkijono
   // Toiminta: muuttaa merkkijonon kirjaimet pieniksi kirjaimiksi
   // Paluuarvo:  -
   void pieniksi( string& mjono );
   
   // Funktio: void tulostaTilastot(unsigned int kierros, 
   //          list<Pelaaja*>& pelaajat, const bool loppuiko);
   // Parametrit: kierrosnumero, pelaajalista, loppuiko peli
   // Toiminta:  tulostaa tilastolistauksen kaikilta pelaajilta
   // Paluuarvo: -
   void tulostaTilastot(unsigned int kierros, list<Pelaaja*>& pelaajat, 
                        bool loppuiko);
   
}

#endif
