//*********************************************************************
// 199006 Tomi Haapaniemi
// haapani8
// 
// Ohjelmointi 2e
// Harjoitusty�
// 
// KomentoKehoite-moduuli
// huolehtii komentokehoitteen toiminnasta
//*********************************************************************

#ifndef KOMENTOKEHOITE_HH
#define KOMENTOKEHOITE_HH


#include <iostream>
#include <cstdlib>
#include <vector>
#include<list>
#include <sstream>
#include <string>
#include <cctype>
#include "pelaaja.hh"
#include "yleisfunktiot.hh"

namespace koke{

//typedeffaukset
typedef list<Pelaaja*>::iterator itPelaaja;
typedef list<Pelaaja*>::iterator const_itPelaaja;

// Komentokehoitteen k�skyjen enumeraatiot
enum Kasky{
   LOPETA, HELP, APUA, TIEDOT, OMA, KARTTA, POMMITA, VAKOILE, KOMENTOKETJU, 
     PAUSE, TILASTOT,
     KVIRHE, LPVIRHE, LVVIRHE, KLPVIRHE, TYHJA
};

// Parametrien tyyppi (ei tyyppi�, �nteger, string)
enum Ptyyppi{
   EI, I, STR
};

// K�skyn suorittamisen j�lkeen teht�v� toimenpide
// (lopeta, jatka t�ll� pelaajalla, vaihda pelaajaa)
enum Palautus{
   LOPPU, JATKA, VAIHDA
};

//Yksitt�isen k�skyn sis�lt�
struct Komento{
   string nimi;
   Kasky komento;
   Ptyyppi tyyppi;
   unsigned int min_parametreja;
   unsigned int max_parametreja;
   Palautus jatketaanko;
};

//K�skyjen alustukset samassa j�rjestyksess� kuin K�sky-enumeraatiot
const Komento KOMENNOT[] = {
   {"lopeta", LOPETA, EI, 0,0, LOPPU},
   {"help", HELP, EI, 0,0, JATKA},
   {"apua", HELP, EI, 0,0, JATKA},
   {"tiedot", TIEDOT, STR, 0,1, JATKA},
   {"oma", OMA, EI, 0,0,  JATKA},
   {"kartta", KARTTA, EI, 0,0,  JATKA},
   {"pommita", POMMITA, I, 2,2, VAIHDA},
   {"vakoile", VAKOILE, I, 2,2, VAIHDA},
   {"komentoketju", KOMENTOKETJU, STR, 1,1, JATKA},
   {"pause", PAUSE, STR, 1,1, JATKA},
   {"tilastot", TILASTOT, EI, 0,0, JATKA},
   {"", KVIRHE, EI, 0,0, JATKA},
   {"", LPVIRHE, EI, 0,0, JATKA},
   {"", LVVIRHE, EI, 0,0, JATKA},
   {"", KLPVIRHE, EI, 0,0, JATKA},
   {"", TYHJA, EI, 0,0, JATKA}
   
};
const int KOMENTOJA = sizeof(KOMENNOT) / sizeof(Komento);

// Funktio: bool lueKomento(Kasky& komento, vector <string>& parametrit);
// Parametrit: komento ja parametrit
// Toiminta: lukee komennon ja parametrit
// Paluuarvo: onnistuiko luku
bool lueKomento(Kasky& komento, vector <string>& parametrit);

// Funktio: Kasky tarkistaKomento(string& komento);
// Parametrit: komento stringin�
// Toiminta: tarkistaa l�ytyyk� annettua komentoa
// Paluuarvo: annetun komennon enumeraatio jos moinen l�ytyy
Kasky tarkistaKomento(string& komento);

// Funktio: void tarkistaParametrit(Kasky& komento, 
//          vector<string>& parametrit);
// Parametrit: komento, komennon parametrit
// Toiminta: tarkistaa ovatko komennolle annetut parametrit laillisia
// Paluuarvo: -
void tarkistaParametrit(Kasky& komento, vector<string>& parametrit);

// Funktio: Palautus suoritaKomento(unsigned int& kierros, 
//          list<Pelaaja*>& pelaajat, list<Pelaaja*>& kaikkiPelaajat, 
//          Kasky& komento, vector<string>& parametrit);
// Parametrit: mones kierros, pelaajalista, pelaajalista kaikista pelaajista, 
//             komento ja komennon parametrit
// Toiminta: suorittaa komennon
// Paluuarvo: mit� tehd��n seuraavaksi (jatketaan samalla pelaajalla, 
//            vaihdetaan pelaajaa tai lopetetaan)
Palautus suoritaKomento(unsigned int& kierros, list<Pelaaja*>& pelaajat, 
                        list<Pelaaja*>& kaikkiPelaajat, Kasky& komento, 
                        vector<string>& parametrit);

// Funktio: void tulostaAputeksti();
// Parametrit: -
// Toiminta: tulostaa aputekstin komentokehoitteelle cout-virtaan
// Paluuarvo: -
void tulostaAputeksti();

// Funktio: bool pause(const string& salasana);
// Parametrit: salasana
// Toiminta: pausettaa pelin PAUSE-komennon m��ritt�m�ll� tavalla
// Paluuarvo: sy�tteen jatkuminen
bool pause(const string& salasana);

}

#endif
